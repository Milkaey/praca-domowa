-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 10 Sty 2022, 09:24
-- Wersja serwera: 10.4.21-MariaDB
-- Wersja PHP: 8.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `test`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pytania`
--

CREATE TABLE `pytania` (
  `id` int(11) NOT NULL,
  `tresc_pytania` text COLLATE utf8_polish_ci NOT NULL,
  `a` varchar(250) COLLATE utf8_polish_ci NOT NULL,
  `b` varchar(250) COLLATE utf8_polish_ci NOT NULL,
  `c` varchar(250) COLLATE utf8_polish_ci NOT NULL,
  `d` varchar(250) COLLATE utf8_polish_ci NOT NULL,
  `prawidlowa_opdpowiedz` varchar(250) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `pytania`
--

INSERT INTO `pytania` (`id`, `tresc_pytania`, `a`, `b`, `c`, `d`, `prawidlowa_opdpowiedz`) VALUES
(1, 'Przedstawiony blok reprezentuje czynność?', ' zastosowania gotowej procedury lub funkcji.', 'wczytania lub wyświetlenia danych.', 'wykonania zadania w pętli.', 'podjęcia decyzji.', 'D'),
(2, 'Aby zadeklarować pole klasy, do którego mają dostęp jedynie metody tej klasy i pole to nie jest dostępne dla\r\nklas pochodnych, należy użyć kwalifikatora dostępu?', 'public.', 'private', 'protected.', 'published.', 'B'),
(3, 'Pętla while powinna być wykonywana tak długo, jak długo zmienna x będzie przyjmowała wartości\r\nz przedziału obustronnie otwartego (-2, 5). Zapis tego warunku w nagłówku pętli za pomocą języka PHP ma\r\npostać?', '($x > -2) && ($x < 5)', '($x == -2) && ($x < 5)\r\n', '($x < -2) || ($x > 5)\r\n', '($x > -2) || ($x > 5)', 'A'),
(4, '…\r\nint zmienna1 = 158;\r\nint *zmienna2 = &zmienna1;\r\n\r\nPo wykonaniu się przedstawionego fragmentu kodu języka C/Ctt zmiennej o nazwie zmienna2 zostanie', 'przypisany adres zmiennej o nazwie zmienna1.', 'przypisana ta sama wartość, co przechowywana w zmienna1.', 'przypisana zamieniona na łańcuch wartość przechowywana w zmienna1.\r\n', 'przypisana liczba w kodzie binarnym odpowiadająca wartości przechowywanej\r\nw zmienna1.', 'A'),
(5, 'W języku PHP float reprezentuje typ?', 'logiczny.\r\n', 'całkowity.', 'łańcuchowy.', 'zmiennoprzecinkowy.\r\n', 'D'),
(6, 'Którym słowem kluczowym, w języku z rodziny C należy posłużyć się, aby przypisać alternatywną nazwę\r\ndla istniejącego typu danych?', 'enum', 'union ', 'switch', 'typedef\r\n', 'D'),
(7, 'Instrukcja for może być zastąpiona instrukcją', 'case\r\n', 'while', 'switch', 'continue', 'B'),
(8, 'int main()\r\n{\r\n int liczba;\r\n cin >> liczba;\r\n while (liczba != 0)\r\n {\r\n if ((liczba % 2) == 0)\r\n cout << liczba << endl;\r\n cin >> liczba;\r\n }\r\n return 0;\r\n}\r\n\r\nPrzedstawiony kod źródłowy, zapisany w języku C++, ma za zadanie dla wprowadzanych dowolnych\r\ncałkowitych liczb różnych od zera wypisać\r\n', 'liczby pierwsze.\r\n', 'wszystkie liczby', 'tylko liczby parzyste.\r\n', 'tylko liczby nieparzyste.', 'C'),
(9, 'DOM dostarcza metod i własności, które w języku JavaScript pozwalają na', 'manipulowanie zadeklarowanymi w kodzie łańcuchami.\r\n', ' wysłanie danych formularza bezpośrednio do bazy danych.\r\n', ' wykonywanie operacji na zmiennych przechowujących liczby.\r\n', ' pobieranie i modyfikowanie elementów strony wyświetlonej przez przeglądarkę.', 'D'),
(10, 'Testy dotyczące skalowalności oprogramowania mają za zadanie sprawdzić, czy aplikacja?', 'ma odpowiednią funkcjonalność.', 'jest odpowiednio udokumentowana.', 'potrafi działać przy zakładanym i większym obciążeniu.', 'jest zabezpieczona przed niedozwolonymi operacjami, np. dzielenie przez zero.', 'C');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `pytania`
--
ALTER TABLE `pytania`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
